from . import views
from django.urls import path

app_name="recommend"

urlpatterns = [
    path('', views.index, name='index'),
    path('channels/', views.getChannelsByCategories, name='channels'),
    path('categories/', views.getAllCategories, name='categories'),
]