from django.db import models

# Create your models here.

class ChannelRatings(models.Model):
    user_id = models.CharField(max_length=200)
    channel_id = models.CharField(max_length=200)
    rating = models.SmallIntegerField()