from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.views.decorators.csrf import ensure_csrf_cookie
import requests
import json

# Create your views here.

# Twitch client ID


client_id = "b0vbe01ed8utjrefvkygvirch1i35v"

def index(request):
    return render(request, 'recommend/all_channels.html')

def getChannelsByCategories(request):

    # Get all the streamers that have recently streamed their selected games
    try:

        favs = request.POST.getlist("categories[]")
        num = 10

    except ValueError as e:
        print("Failed to decode ", e)

    return HttpResponseRedirect(reverse('recommend:index'))

@ensure_csrf_cookie
def getAllCategories(request):

    try:
        response = requests.get(url="https://api.twitch.tv/helix/games/top", headers={"Client-ID": client_id})
    except requests.exceptions.RequestException as e:
        return render(request, 'recommend/all_categories.html')
    else:
        cats = response.json()['data']

        width = "150"
        height = "200"
        for cat in cats:
            cat['box_art_url'] = cat['box_art_url'].replace("{width}", width)
            cat['box_art_url'] = cat['box_art_url'].replace("{height}", height)

        categories = {'categories': cats}

        return render(request, 'recommend/all_categories.html', categories)
